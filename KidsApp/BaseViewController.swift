//
//  BaseViewController.swift
//  KidsApp
//
//  Created by Devanshu Saini on 17/03/18.
//  Copyright © 2018 Devanshu Saini. All rights reserved.
//

import UIKit
import Toast_Swift

class BaseViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    public lazy var doneToolbar = UIToolbar()
    public lazy var pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    public func showLoader() {
        Utils.showLoader(nil)
    }
    
    public func hideLoader() {
        Utils.hideLoader()
    }
    
    func setNavigationTitle(title:String){
        self.navigationItem.title = title
    }
    
    public func configureDefaultDoneToolBar() {
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.toolBarDoneAction(_:)))
        self.doneToolbar.items = [flex, doneButton]
        self.doneToolbar.backgroundColor = .white
        self.doneToolbar.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0)
    }
    
    @objc public func toolBarDoneAction(_ sender: Any) {
        
    }
    
    public func showErrorAlert(_ message:String) {
        var style = ToastStyle()
        style.backgroundColor = UIColor.themeRed
        style.messageFont = UIFont(name: UIFont.kRobotoRegular, size: 12.0)!
        ToastManager.shared.style.verticalPadding = 80.0
        self.view.makeToast(message, position: .top, style: style)
    }
    
    public func showInformationAlert(_ message:String) {
        var style = ToastStyle()
        style.messageFont = UIFont(name: UIFont.kRobotoRegular, size: 12.0)!
        ToastManager.shared.style.verticalPadding = 80.0
        self.view.makeToast(message, position: .top, style: style)
    }
}
