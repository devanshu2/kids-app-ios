//
//  Util.swift
//  Tromsa
//
//  Created by Devanshu Saini on 05/04/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit
import FDKeychain
import CoreTelephony
import Security
import MobileCoreServices
import SVProgressHUD
import Crashlytics
import MaterialComponents.MaterialSnackbar
import Crashlytics

class Utils: NSObject {
    
    static func showLoader(_ loaderString:String?) {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.setForegroundColor(UIColor.themeLightLightBrown)
            SVProgressHUD.setBorderColor(UIColor.themeRed)
            SVProgressHUD.setBorderWidth(1.0)
            SVProgressHUD.show(withStatus: loaderString)
        }
    }
    
    static func hideLoader() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    static func showInfo(_ info:String) {
        //        SVProgressHUD.setDefaultMaskType(.none)
        SVProgressHUD.showInfo(withStatus: info)
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func convertDictionaryToString(_ jsonObject:[String:Any]) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: [])
            // here "jsonData" is the dictionary encoded in JSON data
            return String(data: jsonData, encoding: .utf8)
        } catch {
            debugPrint(error.localizedDescription)
        }
        return nil
    }
    
    static func getDeviceStringFromKeyChain() -> String? {
        var deviceID:String?
        do {
            deviceID = try FDKeychain.item(forKey: "macfarmuuid", forService: "macfarmuuidservice") as? String
        }
        catch {
            debugPrint(error.localizedDescription)
        }
        if deviceID == nil {
            Utils.setDeviceStringFromKeyChain()
            do {
                deviceID = try FDKeychain.item(forKey: "macfarmuuid", forService: "macfarmuuidservice") as? String
            }
            catch {
                debugPrint(error.localizedDescription)
            }
        }
        return deviceID
    }
    
    static func setDeviceStringFromKeyChain() {
        var deviceID = UIDevice.current.identifierForVendor?.uuidString
        if deviceID == nil {
            #if (arch(i386) || arch(x86_64)) && os(iOS)
                deviceID = "SND_EML3BB67-5429-4139-88A9-A6309645688"
            #else
                assert(deviceID != nil, "Unable to get device id")
            #endif
        }
        do {
            try FDKeychain.saveItem(deviceID! as NSCoding, forKey: "macfarmuuid", forService: "macfarmuuidservice")
        }
        catch {
            debugPrint(error.localizedDescription)
        }
    }
    
    static func getCarrierName() -> String? {
        let networkInfo = CTTelephonyNetworkInfo()
        var carrierName:String?
        if let currentCarrier = networkInfo.subscriberCellularProvider {
            carrierName = currentCarrier.carrierName
        }
        return carrierName
    }
    
    static func pushDataToString(_ deviceToken:Data) -> String {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        return tokenParts.joined()
    }
    
    static func handleApiNilResponse(withController vc:UIViewController, theError error: Error?, andRetryCompletion completion: @escaping () -> Swift.Void) {
        var message = "Unable to connect to the server."
        if error != nil {
            if let er = error as NSError? {
                if er.code == NSURLErrorNotConnectedToInternet {
                    message = "No internet connection."
                }
                else if er.code == NSURLErrorNetworkConnectionLost {
                    message = "Internet connection lost."
                }
                else if er.code == NSURLErrorBadURL {
                    message = "Bad request url."
                }
                else if er.code == NSURLErrorUnsupportedURL {
                    message = "Invalid request url."
                }
                else if er.code == NSURLErrorTimedOut {
                    message = "Internet connection lost."
                }
                else if er.code == NSURLErrorCancelled {
                    return
                }
            }
        }
        
        let snackMessage = MDCSnackbarMessage()
        snackMessage.text = message
        
        let action = MDCSnackbarMessageAction()
        let actionHandler = {() in
            completion()
        }
        action.handler = actionHandler
        action.title = "Retry"
        snackMessage.action = action
        MDCSnackbarMessageView.appearance().buttonFont = UIFont(name: UIFont.kRobotoMedium, size: 12.0)
        MDCSnackbarMessageView.appearance().messageFont = UIFont(name: UIFont.kRobotoRegular, size: 12.0)
        MDCSnackbarManager.show(snackMessage)
    }
    
    static func printDictionary(_ dictionary:Dictionary<String, Any>){
        do{
            let data = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                debugPrint(json)
            }
        }catch{
            
        }
    }
    
    static func recordAPIParseIssue(data:Data?, endpoint:String, function:String?, file:String?, line:Int?, otherInfo:String?) {
        var errorString = "Endpoint: " + endpoint
        if data != nil {
            errorString.append(String(format:"\n Data: %@", String(data: data!, encoding: .utf8)!))
        }
        if function != nil {
            errorString.append("\n Function: \(function!)")
        }
        if file != nil {
            errorString.append("\n File: \(file!)")
        }
        if line != nil {
            errorString.append("\n Line: \(line!)")
        }
        if otherInfo != nil {
            errorString.append("\n Line: \(otherInfo!)")
        }
        let error = NSError(domain: NSPOSIXErrorDomain, code: -9999, userInfo: [NSLocalizedDescriptionKey:errorString])
        Crashlytics.sharedInstance().recordError(error)
    }
}

