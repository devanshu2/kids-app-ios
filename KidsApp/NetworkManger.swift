//
//  NetworkManger.swift
//  EventApp
//
//  Created by Devanshu Saini on 14/01/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import Foundation

public class NetworkManger: NSObject {
    
    public var networkSession: URLSession?
    public var networkDataTask: URLSessionDataTask?
    public var isIncognito = false
    public func getSharedConfiguration(_ isEphemeral: Bool) -> URLSessionConfiguration {
        let sessionConfiguration = isEphemeral ? URLSessionConfiguration.ephemeral : URLSessionConfiguration.default
        return sessionConfiguration
    }
    
    public func cancelTask() {
        self.networkDataTask?.cancel()
    }
    
    public func getDefaultHeaders() -> [String:String] {
        var headerParams: Dictionary<String, String>
        headerParams = ["Content-Type": "application/json", "accept":"application/json"]
//        if let userToken = Utils.getAppUser()?.token {
//            headerParams["Authorization"] = "1234567890qwertyuiop"
//        }
        return headerParams
    }
    
    public func postData(withParameters params:[String: Any], apiEndpoint apiURL: String, savePostDataToFile doSave:Bool = false,  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        self.makeServerRequest(requestType: String.kPost, withParameters: params, apiEndpoint: apiURL, savePostDataToFile: doSave, CompletionHandler: completionHandler)
    }
    
    public func putData(withParameters params:[String: Any], apiEndpoint apiURL: String, savePostDataToFile doSave:Bool = false,  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        self.makeServerRequest(requestType: String.kPut, withParameters: params, apiEndpoint: apiURL, savePostDataToFile: doSave, CompletionHandler: completionHandler)
    }
    
    public func getData(withApiEndpoint apiURL: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        self.makeServerRequest(requestType: String.kGet, apiEndpoint: apiURL, CompletionHandler: completionHandler)
    }
    
    public func makeServerRequest(requestType rType:String, withParameters params:[String: Any]? = nil, apiEndpoint apiURL: String, savePostDataToFile doSave:Bool = false, CustomHeaderParams customHeaderParams:[String: String]? = nil, extraHeaderParams headerParams:[String: String]? = nil, forceFormURLEncode formURLEncode:Bool = false,isEncodingASCII encodingASCII:Bool = false, CompletionHandler completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        var request = URLRequest(url: URL(string: apiURL)!)
        request.httpMethod = rType
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        if customHeaderParams == nil {
            for (key, value) in self.getDefaultHeaders() {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        else {
            for (key, value) in customHeaderParams! {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if headerParams != nil {
            for (key, value) in headerParams! {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        
        var paramString:String?
        
        if let sParams = params as? [String:String], formURLEncode {
            var parameterArray = [String]()
            for (key, value) in sParams {
                let el = String(format:"%@=%@", key, value.percentEscapeString())
                parameterArray.append(el)
            }
            paramString = parameterArray.joined(separator: "&")
            
            //For Form URLEncoded the request needs data in ASCII Encoding
            if encodingASCII{
                let postData:Data = (paramString?.data(using: String.Encoding.ascii))!
                let postLength:String = String(postData.count) as String
                request.httpBody = postData
                request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
            }
            else{
                request.httpBody = paramString?.data(using: .utf8)
                //request.httpBody = parameterArray.joinWithSeparator("&").dataUsingEncoding(NSUTF8StringEncoding)
            }
        }
            
        else {
            if params != nil {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params!, options: [])
                    paramString = String(data: jsonData, encoding: .utf8)
                } catch {
                    debugPrint(error.localizedDescription)
                }
            }
            if paramString != nil {
                request.httpBody = paramString?.data(using: .utf8)
            }
        }
        
        if doSave {
            #if DEBUG
            _ = paramString?.saveToFile()
            #endif
        }
        
        if self.networkSession == nil {
            self.networkSession = URLSession(configuration: getSharedConfiguration(self.isIncognito))
        }
        debugPrint("Making \(rType) request with endpoint: \(apiURL)")
        self.networkDataTask = self.networkSession?.dataTask(with: request as URLRequest) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 500 {
                    Utils.recordAPIParseIssue(data: data, endpoint: apiURL, function: #function, file: #file, line: #line, otherInfo:"500 httpstatus")
                }
            }
            if error != nil {
                debugPrint("Got error response for \(rType) request with endpoint: \(apiURL)")
                debugPrint(error!.localizedDescription)
            }
            if data != nil {
                debugPrint("Got response for \(rType) request with endpoint: \(apiURL)")
            }
            completionHandler(data, response, error)
        }
        
        self.networkDataTask?.resume()
    }
}
