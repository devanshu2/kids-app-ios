//
//  Constants.swift
//  SpecialDeal
//
//  Created by Devanshu Saini on 22/11/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit

struct Constants {
    
    struct Storyboards {
        static let Main = "Main"
    }
    
    struct APIConstants {
        static let Base: String = {
            #if DEBUG
                return "http://13.126.138.229/"
            #else
                return "http://13.126.138.229/"
            #endif
        }()
        
        struct EndPoints {
            static func endPointString(_ ep:String) -> String {
                return Constants.APIConstants.Base + "api/web_services/" + ep
            }
            static let places = endPointString("places")
            static let commentPost = endPointString("comment_post")
            static let comments = endPointString("comments")
        }
    }
    
    struct TableViewCellIdentifier {
        static let RatingSectionHeaderView = "RatingSectionHeaderView"
        static let WriteProductReviewFormTableViewCell = "WriteProductReviewFormTableViewCell"
        static let UserReviewTableViewCell = "UserReviewTableViewCell"
        static let RatingPageProductInfoTableViewCell = "RatingPageProductInfoTableViewCell"
        static let ImageCollectionCell = "ImageCollectionCell"
        static let ListDetailHeaderTableViewCell = "ListDetailHeaderTableViewCell"
        static let PlaceDescriptionTableViewCell = "PlaceDescriptionTableViewCell"
    }
    
    struct ViewControllerIdentifiers {
        static let HomeViewController = "HomeViewController"
    }
    
    struct General {
        static let Documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        static let Tmp = NSTemporaryDirectory()
        static let AnimationDuration = 0.4
        static let APIDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //yyyy-MM-dd'T'HH:mm:ssZZZZ
    }
    
    struct Entities {

    }
    
    struct FontNames {
        static let RubikBlack = "Rubik-Black"
        static let RubikBlackItalic = "Rubik-BlackItalic"
        static let RubikBold = "Rubik-Bold"
        static let RubikBoldItalic = "Rubik-BoldItalic"
        static let RubikItalic = "Rubik-Italic"
        static let RubikLight = "Rubik-Light"
        static let RubikLightItalic = "Rubik-LightItalic"
        static let RubikMedium = "Rubik-Medium"
        static let RubikMediumItalic = "Rubik-MediumItalic"
        static let RubikRegular = "Rubik-Regular"
    }
    
    struct Colors {
        
        static let color000000 : UIColor = UIColor.black
        static let color181818 = UIColor(hex:"181818")
        static let color212121 = UIColor(hex:"212121")
        static let color8D8D8D = UIColor(hex:"8D8D8D")
        static let colorDEDEDE = UIColor(hex:"DEDEDE")
        static let colorD0D0D0 = UIColor(hex:"D0D0D0")
        static let colorDDDDDD = UIColor(hex:"DDDDDD")
        static let colorECECEC = UIColor(hex:"ECECEC")
        static let colorE2E2E2 = UIColor(hex:"E2E2E2")
        static let colorD2D2D2 = UIColor(hex:"D2D2d2")
        static let colorFF878B = UIColor(hex:"FF878B")
        static let colorC85BE5 = UIColor(hex:"C85BE5")
        static let colorFD2C7A = UIColor(hex:"FD2C7A")
        static let colorF5F5F5 = UIColor(hex:"F5F5F5")
        static let colorF2F2F2 = UIColor(hex:"F2F2F2")
        static let colorF8F8F8 = UIColor(hex:"F8F8F8")
        static let colorFF7255 = UIColor(hex:"FF7255")
        static let colorFF1744 = UIColor(hex:"FF1744")
        static let color797979 = UIColor(hex:"797979")
        static let color979797 = UIColor(hex:"979797")
        static let color8F8F8F = UIColor(hex:"8F8F8F")
        static let colorFEF6F7 = UIColor(hex:"FEF6F7")
        static let color4A4A4A = UIColor(hex:"4A4A4A")
        static let colorF7B81C = UIColor(hex:"F7B81C")
        static let color18D0B4 = UIColor(hex:"18D0B4")
        static let colorBDBDBD = UIColor(hex:"BDBDBD")
        static let color9B9B9B = UIColor(hex:"9B9B9B")
        static let defaultGreyBG = UIColor(hex:"E9E9E9")
        
        static let whiteColor : UIColor = UIColor.white
        static let clearColor : UIColor = UIColor.clear
        static let redColor : UIColor = UIColor.red
    }
    
    struct Notifications {
        static let ImageFetchNotification = Notification.Name("ImageFetchNotification")
    }
}
