//
//  NSMutableData.swift
//  KP-LMS
//
//  Created by Devanshu Saini on 23/05/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import Foundation

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

extension Data {
    mutating func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
    
    var stringValue:String! {
        get {
            return String(data:self, encoding:.utf8)
        }
    }
}
