//
//  Date.swift
//  KidsApp
//
//  Created by Devanshu Saini on 24/04/18.
//  Copyright © 2018 Devanshu Saini. All rights reserved.
//

import Foundation

extension Date {
    var yearMonthDateStringOutput: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    var monthDayYearTimeStringOutput: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy /hh:mm"
        return dateFormatter.string(from: self)
    }
    
    static func dateFromAPIDateResponse(_ dateResponse:String?) -> Date? {
        if dateResponse == nil {
            return nil
        }
        let df = DateFormatter()
        df.dateFormat = Constants.General.APIDateFormat
        return df.date(from: dateResponse!)
    }
    
    static func getCurrentWeekDay() -> Int {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        // get the weekday of the current date
        let cal = Calendar.current
        var calComp:Set<Calendar.Component> = Set()
        calComp.insert(.weekday)
        let componenets = cal.dateComponents(calComp, from: Date())
        return componenets.weekday!
    }
    
    var minutesSinceMidnight:Int
    {
        get {
            var calComp:Set<Calendar.Component> = Set()
            calComp.insert(.hour)
            calComp.insert(.minute)
            calComp.insert(.second)
            let components = Calendar.current.dateComponents(calComp, from: self)
            return 60 * components.hour! + components.minute!
        }
    }
}
