//
//  UIColor.swift
//  etmoneyios
//
//  Created by Devanshu Saini on 27/12/16.
//  Copyright © 2016 Dew Solutions Private Limited. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
    
    var coreImageColor: CoreImage.CIColor? {
        return CoreImage.CIColor(color: self)  // The resulting Core Image color, or nil
    }
    /**
     88, 49, 50
     */
    class var themeBrown: UIColor {
        return UIColor(red: 88/255, green: 49/255, blue: 50/255, alpha: 1.0)
    }
    
    /**
     255, 73, 80
     */
    class var themeRed: UIColor {
        return UIColor(red: 1.0, green: 73/255, blue: 80/255, alpha: 1.0)
    }
    
    /**
     236, 224, 219
     */
    class var themeLightLightBrown: UIColor {
        return UIColor(red: 236/255, green: 224/255, blue: 219/255, alpha: 1.0)
    }
    
    /**
     c2c2c2
     */
    class var borderColor: UIColor {
        return UIColor(hex: "c2c2c2")
    }
    /**
     2cc0c2
     */
    class var themeTeal: UIColor {
        return UIColor(hex: "2cc0c2")
    }
    /**
     249d9f
     */
    class var themeDarkTeal: UIColor {
        return UIColor(hex: "249d9f")
    }
    /**
     e54d8c
     */
    class var themeDarkishPink: UIColor {
        return UIColor(hex: "e54d8c")
    }
    /**
     dca72e
     */
    class var themeGoldenYellow: UIColor {
        return UIColor(hex: "dca72e")
    }
    /**
     e9b53d
     */
    class var themeDarkYellow: UIColor {
        return UIColor(hex: "e9b53d")
    }
    
    /**
     323232
     */
    class var themePlainBlack: UIColor {
        return UIColor(hex: "323232")
    }
    
    /**
     2a2d3c
     */
    class var themeDarkishBlack: UIColor {
        return UIColor(hex: "2a2d3c")
    }
/**
 515360
 */
    class var themeNormalBlack: UIColor {
        return UIColor(hex: "515360")
    }
    
    /**
     686c77
     */
    class var themeLightBlack: UIColor {
        return UIColor(hex: "686c77")
    }
    
    /**
     c6c6c6
     */
    class var themeLightGray: UIColor {
        return UIColor(hex:"c6c6c6")
    }
    
    /**
     eae9ef
     */
    class var themeLightGray2: UIColor {
        return UIColor(hex:"eae9ef")
    }
    
    /**
     737373
     */
    class var themeGray: UIColor {
        return UIColor(hex:"737373")
    }
    /**
     f2f2f2
     */
    class var themeLineColor: UIColor {
        return UIColor(hex: "f2f2f2")
    }
    
    /**
     aa87cc
     */
    class var themePurpleColor: UIColor {
        return UIColor(hex: "aa87cc")
    }
    
    /**
     41f9fe
     */
    class var themeSkyBlueColor: UIColor {
        return UIColor(hex: "41f9fe")
    }
}
