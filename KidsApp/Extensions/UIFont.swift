//
//  UIFont.swift
//  KP-LMS
//
//  Created by Devanshu Saini on 25/09/17.
//  Copyright © 2017 Dew Solutions Pvt Ltd. All rights reserved.
//

import UIKit

extension UIFont {
    static var kRobotoRegular = "Roboto-Regular"
    static var kRobotoMedium = "Roboto-Medium"
    static var kRobotoBold = "Roboto-Bold"
    
    static var themeFontA:UIFont {
        get {
            return UIFont(name: UIFont.kRobotoRegular, size: 15.0)!
        }
    }
    static var theThemeFontB:UIFont {
        get {
            return UIFont(name: UIFont.kRobotoMedium, size: 14.0)!
        }
    }
}
