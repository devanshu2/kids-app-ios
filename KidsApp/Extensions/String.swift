//
//  StringExtension.swift
//  etmoneyios
//
//  Created by Devanshu Saini on 27/12/16.
//  Copyright © 2016 Dew Solutions Private Limited. All rights reserved.
//

import Foundation
import Gloss

extension String {
    
    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let test =  emailTest.evaluate(with: self)
        debugPrint(test)
        return test
    }
    
    func isVaildPhoneNumber() -> Bool {
        let test = !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        if test == true {
            if self.length > 6 && self.length < 14 {
                return true
            }
        }
        return false
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }
    
    static func isValid(paramString theString:Any?) ->Bool {
        if theString != nil {
            if theString is String {
                let x = theString as! String
                let trimmedString = x.trimmingCharacters(in: .whitespaces)
                if (trimmedString.length) > 0 {
                    return true
                }
            }
        }
        return false
    }
    
    static func isUnsafeValid(paramString theString:Any?) ->Bool {
        if theString != nil {
            if theString is String {
                return true
            }
        }
        return false
    }

    static func isValidURL(paramURLString theURLString: String?) -> Bool {
        if String.isValid(paramString: theURLString) {
            if let candidateURL = URL(string: theURLString!) {
                if ( (candidateURL.scheme != nil) && (candidateURL.host != nil) ) {
                    return true
                }
            }
        }
        return false
    }
    
    static func isValidateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    static func getRandomString(_ length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func textRect(withFont font: UIFont) -> CGRect {
        return self.textRect(withFont: font, andWidth: CGFloat.greatestFiniteMagnitude, andHeight: CGFloat.greatestFiniteMagnitude)
    }
    
    func textRect(withFont font: UIFont, andWidth width:CGFloat) -> CGRect {
        return self.textRect(withFont: font, andWidth: width, andHeight: CGFloat.greatestFiniteMagnitude)
    }
    
    func textRect(withFont font: UIFont, andHeight height:CGFloat) -> CGRect {
        return self.textRect(withFont: font, andWidth: CGFloat.greatestFiniteMagnitude, andHeight: height)
    }
    
    func textRect(withFont font: UIFont, andWidth width:CGFloat, andHeight height:CGFloat) -> CGRect {
        let constraintRect = CGSize(width: width, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle: NSParagraphStyle.default], context: nil)
        return boundingBox
    }
    func withBoldText(boldPartsOfString: Array<String>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedStringKey.font:font!]
        let boldFontAttribute = [NSAttributedStringKey.font:boldFont!]
        let boldString = NSMutableAttributedString(string: self as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: (self.lowercased() as NSString).range(of: boldPartsOfString[i].lowercased() as String))
        }
        return boldString
    }
    func JSONValue() -> AnyObject?
    {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if let jsonData = data
        {do{
            let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
            if let jsonResult = message as? JSON
            {
                return jsonResult as AnyObject?
            }
            if let jsonArray = message as? Array<JSON>
            {
                   return jsonArray as AnyObject
            }
            if let stringArray = message as? Array<String>
            {
                return stringArray as AnyObject
            }
            else
            {
                return nil
            }
        }
        catch _ as NSError{return nil}}else{return nil}
    }
    func chopPrefix(_ count: Int = 1) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: count))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        return self.substring(to: self.characters.index(self.endIndex, offsetBy: -count))
    }
    
    func toJSONArray() throws -> [Dictionary<String, Any>] {
        let data = self.data(using: .utf8)
        let objectArray = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [Dictionary<String, Any>]
        return objectArray
    }
    
    func toCamelCase() -> String {
        let trimmedString = self.trimmingCharacters(in: .whitespacesAndNewlines);
        let parts = trimmedString.characters.split{$0 == " "}.map(String.init)
        var camelCaseString = ""
        for str in parts {
            camelCaseString.append(str.substring(with: startIndex..<str.index(startIndex, offsetBy: 1)).uppercased())
            camelCaseString.append(str.substring(from: str.index(startIndex, offsetBy: 1)))
            if camelCaseString.length != trimmedString.length {
                camelCaseString.append(" ")
            }
        }
        return camelCaseString
    }

        
    var digits: String {
            return components(separatedBy: CharacterSet.decimalDigits.inverted)
                .joined(separator: "")
    }
    
    func concatStringWithSpace(_ str:String?) -> String {
        if str == nil || str?.length == 0 {
            return self
        }
        else {
            return String(format:"%@ %@", self, str!)
        }
    }
    
    func urlQueryString() -> String? {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    func saveToFile() -> String? {
        let d = String(Date().timeIntervalSince1970)
        var path = Constants.General.Documents + "/logs"
        do {
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
        path = path + "/" + d + ".txt"
        do {
            try self.write(toFile: path, atomically: true, encoding: .utf8)
            debugPrint("Post params logged to file: " + path)
        }
        catch {
            debugPrint(error.localizedDescription)
            return nil
        }
        return path
    }
    
    func percentEscapeString() -> String! {
        let characterSet = NSCharacterSet.alphanumerics as! NSMutableCharacterSet
        characterSet.addCharacters(in: "-._* ")
        return self.addingPercentEncoding(withAllowedCharacters: characterSet as CharacterSet)?.replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    //MARK: Combine String
    func combine(firstString: String?, secondString: String?) -> String {
        return [firstString, secondString].flatMap{$0}.joined(separator: " ")
    }
    
    //MARK:- String Constants
    static let kPut = "PUT"
    static let kPost = "POST"
    static let kGet = "GET"
    static let kDelete = "DELETE"
    static let kPrincipal = "principal"
    static let kPassword = "password"
    static let kDeviceID = "deviceId"
    static let kDeviceUser = "deviceUser"
    static let kConnectionID = "connectionId"
    static let kPage = "page"
    static let kSize = "size"
    static let kPushToken = "MacFarmPushToken"
    static let kImageURL = "kImageURL"
    static let kImage = "kImage"
}
