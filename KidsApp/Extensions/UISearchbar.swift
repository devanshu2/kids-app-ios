//
//  UISearchbar.swift
//  Tromsa
//
//  Created by Devanshu Saini on 15/08/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit

extension UISearchBar {
    var textColor:UIColor? {
        get {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                return textField.textColor
            } else {
                return nil
            }
        }
        
        set (newValue) {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                textField.textColor = newValue
            }
        }
    }
}
