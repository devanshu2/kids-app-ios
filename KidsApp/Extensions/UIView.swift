//
//  UIView.swift
//  etmoneyios
//
//  Created by Devanshu Saini on 15/02/17.
//  Copyright © 2017 Dew Solutions Private Limited. All rights reserved.
//

import UIKit

//MARK: Extension to get the image out of a UIView

extension UIView{
    var screenshot: UIImage{
        self.setNeedsLayout()
        self.layoutIfNeeded()
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext();
        self.layer.render(in: context!)
        let screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return screenShot!
    }
    
    func prepareForCustomConstraints() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
//    // MARK:- For Border Color of View
//    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
//        let border = KPCALayer()
//        border.backgroundColor = color.cgColor
//        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
//        self.layer.addSublayer(border)
//    }
//
//    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
//        let border = KPCALayer()
//        border.backgroundColor = color.cgColor
//        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
//        self.layer.addSublayer(border)
//    }
//
//    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
//        let border = KPCALayer()
//        border.backgroundColor = color.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
//        self.layer.addSublayer(border)
//    }
//
//    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
//        let border = KPCALayer()
//        border.backgroundColor = color.cgColor
//        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
//        self.layer.addSublayer(border)
//    }
}

extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5 * self.frame.size.width))/self.frame.width)+1
    }
}
