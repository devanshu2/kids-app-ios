//
//  HomeViewController.swift
//  KidsApp
//
//  Created by Devanshu Saini on 14/12/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit
import SDWebImage

class ListItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}

class ListViewController: BaseViewController {

    @IBOutlet weak var collectionView:UICollectionView!
    public var places:[PlaceItem]!
    public var topTitle:String?
    var cellSpacing:CGFloat!
    var leftRightEdges:CGFloat!
    let cellEdge:CGFloat = 140.0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.calculateCellSizes()
        self.title = self.topTitle
    }
    
    private func calculateCellSizes() {
        let viewWidth = self.view.bounds.size.width
        let dx:CGFloat = 10.0
        var cellCount:CGFloat = 2
        while true {
            let w1 = (cellCount * self.cellEdge) + ((cellCount + 1.0) * dx)
            let w2 = viewWidth - w1
            if w2 >= (self.cellEdge + dx) {
                cellCount = cellCount + 1
            }
            else {
                let e15:CGFloat = 15.0
                let w3 = (cellCount * self.cellEdge) + ((cellCount + 1.0) * e15)
                if viewWidth >= w3 {
                    self.cellSpacing = 15.0
                }
                else {
                    self.cellSpacing = 10.0
                }
                let w4 = (cellCount * self.cellEdge) + ((cellCount - 1) * self.cellSpacing)
                self.leftRightEdges = (viewWidth - w4)/2
                break
            }
        }
        self.collectionView.reloadData()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        self.calculateCellSizes()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DetailViewController, let place = sender as? PlaceItem {
            vc.place = place
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "listToDetail", sender: self.places[indexPath.item]) //homeToDetail
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cellEdge, height: self.cellEdge)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: self.cellSpacing, left: self.leftRightEdges, bottom: self.cellSpacing, right: self.leftRightEdges)
    }
    
    //vertical space between rows
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.cellSpacing
    }
    
    //between cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return self.cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return .zero
    }
}

// MARK: - UICollectionViewDataSource

extension ListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.places.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ListItemCollectionViewCell
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1.0
        let place = self.places[indexPath.item]
        cell.titleLabel.text = place.placeName
        var imgURL: URL?
        if let imgs = place.featuredMarkerImage?.imageURLString {
            imgURL = URL(string: imgs)
        }
        cell.imageView.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "default"))
        return cell
    }
}

