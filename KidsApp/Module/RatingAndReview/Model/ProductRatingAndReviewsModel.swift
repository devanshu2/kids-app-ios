//
//  ProductRatingAndReviewsModel.swift
//  TataCLiQ
//
//  Created by Devanshu Saini on 29/03/18.
//  Copyright © 2018 Dew Solutions Private Limited. All rights reserved.
//

import Foundation
import Gloss

public enum RatingReviewsOrder {
    case byDateAscending
    case byDateDescending
    case byRatingAscending
    case byRatingDescending
    
    func getDisplayString() -> String {
        switch self {
        case .byDateAscending:
            return "Oldest first"
        case .byDateDescending:
            return "Newest first"
        case .byRatingAscending:
            return "Negative first"
        default:
            return "Positive first"
        }
    }
}

public class ProductReview : BaseResponse {
    public var isJustFreshOne = false
    public var isCanEditDelete:Bool? = false
    public var alias:String?
    public var comment:String?
    public var reviewDate:Date?
    public var headline:String?
    public var reviewID:String?
    public var rating: Int?
    
    required public init?(json: JSON) {
        super.init(json: json)
        self.isCanEditDelete = "isCanEditDelete" <~~ json
        self.alias = "alias" <~~ json
        self.comment = "comment" <~~ json
        let rDateString:String? = "date" <~~ json
        self.reviewDate = Date.dateFromAPIDateResponse(rDateString)
        self.headline = "headline" <~~ json
        self.reviewID = "id" <~~ json
        self.rating = "rating" <~~ json
        if self.rating == nil {
            self.rating = 0
        }
    }
}

public class ProductReviewsResponse : BaseResponse {
    public var pageNumber: Int?
    public var pageSize: Int?
    public var totalNoOfPages: Int?
    public var totalNoOfReviews: Int?
    public var reviews: [ProductReview]?
    
    required public init?(json: JSON) {
        super.init(json: json)
        self.pageNumber = "pageNumber" <~~ json
        self.pageSize = "pageSize" <~~ json
        self.totalNoOfPages = "totalNoOfPages" <~~ json
        self.totalNoOfReviews = "totalNoOfReviews" <~~ json
        self.reviews = "reviews" <~~ json
    }
}

class ProductRatingAndReviewsRequest: BaseModel {
    
    private var _productID:String!
    public var productID:String! {
        set {
            self._productID = newValue
        }
        get {
            return self._productID.uppercased()
        }
    }
    
    
    init(productID:String) {
        super.init()
        self.productID = productID
    }
    
    public func editOrCreateReview(_ reviewTitle: String, ReviewDescription reviewDescription:String, Rating rating:Int, ReviewID reviewID:Int? = nil, CompletionHandler completionHandler: @escaping(ProductReview?, Error?, Int?) -> Swift.Void) {
        let postParams:[String:String] = ["headline":reviewTitle, "rating":String(rating), "comment": reviewDescription]
        var apiEndPoint = Constants.APIConstants.EndPoints.commentPost
        if reviewID != nil {
            apiEndPoint = apiEndPoint.appendingFormat("&id=%ld", reviewID!)
        }
        self.networkManager.makeServerRequest(requestType: String.kPost, withParameters: postParams, apiEndpoint: apiEndPoint, extraHeaderParams: ["Content-Type":"application/x-www-form-urlencoded"], forceFormURLEncode:true, CompletionHandler: { (data, response, error) in
            var responseStatusCode: Int?
            if let httpResponse = response as? HTTPURLResponse {
                responseStatusCode = httpResponse.statusCode
            }
            if error != nil {
                DispatchQueue.main.async {
                    completionHandler(nil, error, responseStatusCode)
                }
            } else {
                guard let jsonDictionary = GlossJSONSerializer().json(from: data!, options: .mutableContainers) else {
                    self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
                    debugPrint("Unable to convert JSON")
                    DispatchQueue.main.async {
                        completionHandler(nil, error, responseStatusCode)
                    }
                    return
                }
                if let object = ProductReview(json: jsonDictionary) {
                    DispatchQueue.main.async {
                        completionHandler(object, nil, responseStatusCode)
                    }
                } else {
                    self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
                    debugPrint("Error is: \(error?.localizedDescription ?? "Error Found")")
                    DispatchQueue.main.async {
                        completionHandler(nil, error, responseStatusCode)
                    }
                }
            }
        })
    }
    
    
    //    public func deleteReview(_ reviewID: String, CompletionHandler completionHandler: @escaping(BaseResponse?, Error?, Int?) -> Swift.Void) {
    //                let apiEndPoint = ""
    //                self.networkManager.getData(withApiEndpoint: apiEndPoint, completionHandler: { (data, response, error) in
    //                    var responseStatusCode: Int?
    //                    if let httpResponse = response as? HTTPURLResponse {
    //                        responseStatusCode = httpResponse.statusCode
    //                    }
    //                    if error != nil {
    //                        DispatchQueue.main.async {
    //                            completionHandler(nil, error, responseStatusCode)
    //                        }
    //                    } else {
    //                        guard let jsonDictionary = GlossJSONSerializer().json(from: data!, options: .mutableContainers) else {
    //                            self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
    //                            debugPrint("Unable to convert JSON")
    //                            DispatchQueue.main.async {
    //                                completionHandler(nil, error, responseStatusCode)
    //                            }
    //                            return
    //                        }
    //                        if let object = GenericResponse(json: jsonDictionary) {
    //                            DispatchQueue.main.async {
    //                                completionHandler(object, nil, responseStatusCode)
    //                            }
    //                        } else {
    //                            self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
    //                            debugPrint("Error is: \(error?.localizedDescription ?? "Error Found")")
    //                            DispatchQueue.main.async {
    //                                completionHandler(nil, error, responseStatusCode)
    //                            }
    //                        }
    //                    }
    //                })
    //        }
    //    }
    
    public func getReviews(PageNumber pageNumber: Int, sortOrderType orderType:RatingReviewsOrder, CompletionHandler completionHandler: @escaping(ProductReviewsResponse?, Error?, Int?) -> Swift.Void) {
        var userPartString = "/users/anonymous"
        //        if AppDelegate.appDelegate().isUserLogedIn {
        //            userPartString = String(format:"/users/%@", AppDelegate.appDelegate().customerLoginId)
        //        }
        var sortParams = ""
        switch orderType {
        case .byDateAscending:
            sortParams = "orderBy=asc&sort=byDate"
        case .byDateDescending:
            sortParams = "orderBy=desc&sort=byDate"
        case .byRatingAscending:
            sortParams = "orderBy=asc&sort=byRating"
        default:
            sortParams = "orderBy=desc&sort=byRating"
        }
        let apiEndPoint = ""
        self.networkManager.getData(withApiEndpoint: apiEndPoint, completionHandler: { (data, response, error) in
            var responseStatusCode: Int?
            if let httpResponse = response as? HTTPURLResponse {
                responseStatusCode = httpResponse.statusCode
            }
            if error != nil {
                DispatchQueue.main.async {
                    completionHandler(nil, error, responseStatusCode)
                }
            } else {
                guard let jsonDictionary = GlossJSONSerializer().json(from: data!, options: .mutableContainers) else {
                    self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
                    debugPrint("Unable to convert JSON")
                    DispatchQueue.main.async {
                        completionHandler(nil, error, responseStatusCode)
                    }
                    return
                }
                if let object = ProductReviewsResponse(json: jsonDictionary) {
                    DispatchQueue.main.async {
                        completionHandler(object, nil, responseStatusCode)
                    }
                } else {
                    self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
                    debugPrint("Error is: \(error?.localizedDescription ?? "Error Found")")
                    DispatchQueue.main.async {
                        completionHandler(nil, error, responseStatusCode)
                    }
                }
            }
        })
    }
}


