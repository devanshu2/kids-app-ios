//
//  ProductRatingAndReviewsViewController.swift
//  TataCLiQ
//
//  Created by Devanshu Saini on 29/03/18.
//  Copyright © 2018 Dew Solutions Private Limited. All rights reserved.
//

import UIKit
import Cosmos
import UITextView_Placeholder

//protocol ProductRatingAndReviewsViewControllerDelegate : class {
//    func addProductTocart(productID: String, ussid: String, otherSellers : TCProductOtherSellers?)
//    func addProductToWishlist(productID: String, ussid: String, otherSellers : TCProductOtherSellers?)
//}

class TCProductDetail {
    var productListingId:String?
}

class RatingAndReviewsViewController: BaseViewController {

    public var productDetail:TCProductDetail!
//    public var bottomViewHidden:Bool = false
//    @IBOutlet weak var bottomViewBottom:NSLayoutConstraint!
//    @IBOutlet weak var addToBagButton: UIButton!
//    @IBOutlet weak var addToWishlistButton: UIButton!
//    @IBOutlet weak var addtoWishlistButtonTitleLabel: UILabel!
//    @IBOutlet weak var addToCartButtonTitleLabel: UILabel!
    
//    weak var delegate: ProductRatingAndReviewsViewControllerDelegate?
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var tableViewBottom:NSLayoutConstraint!

    private var sortSetting = RatingReviewsOrder.byRatingDescending
    private var isOnNetworkCall = false
    private var isMainRatingFormOpen = false
    private var isFirstCallDone = false
    private var currentPageNumer = 0
    private var totalNumberOfReviews = 0
    private var reviews:[ProductReview] = []
    private var request:ProductRatingAndReviewsRequest!
    lazy var userReview = ProductReview(json: [:])
    private var rHeaderView:RatingSectionHeaderView?
    private lazy var orderOptions:[RatingReviewsOrder] = [RatingReviewsOrder.byDateDescending, RatingReviewsOrder.byRatingDescending, RatingReviewsOrder.byRatingAscending, RatingReviewsOrder.byDateAscending]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.request = ProductRatingAndReviewsRequest(productID: self.productDetail.productListingId!)
        debugPrint("ProductRatingAndReviews: \(self.productDetail.productListingId!)")
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.setNavigationTitle(title: Constants.ViewControllerTitle.RatingsAndReview)
        self.callApi(withLoader: true, pageNumber: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        let keyboardInfo = notification.userInfo
        let keyboardFrameBegin = keyboardInfo?[UIKeyboardFrameEndUserInfoKey]
        let keyboardFrameBeginRect = (keyboardFrameBegin as! NSValue).cgRectValue
        DispatchQueue.main.async {
            self.tableViewBottom.constant = keyboardFrameBeginRect.size.height
            UIView.animate(withDuration: Constants.General.AnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        DispatchQueue.main.async {
            self.tableViewBottom.constant = 0.0
            UIView.animate(withDuration: Constants.General.AnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    private func callApi(withLoader:Bool, pageNumber:Int) {
        if withLoader {
            self.showLoader()
        }
        self.isOnNetworkCall = true
        self.request.getReviews(PageNumber: pageNumber, sortOrderType: self.sortSetting) { [weak self] (reviewResponse, error, httpStatusCode) in
            if self != nil {
                self?.hideLoader()
                if error != nil || reviewResponse == nil {
                    self!.isOnNetworkCall = false
                    Utils.handleApiNilResponse(withController: self!, theError: error, andRetryCompletion: {
                        self?.callApi(withLoader: withLoader, pageNumber: pageNumber)
                    })
                }
                else {
                    self?.processValidResponseForAPI(reviewResponse: reviewResponse!, pageNumber: pageNumber)
                }
            }
        }
    }
    
    private func processValidResponseForAPI(reviewResponse:ProductReviewsResponse, pageNumber:Int) {
        if let responsePageNumber = reviewResponse.pageNumber, let responseTotalNoOfReviews = reviewResponse.totalNoOfReviews, responsePageNumber == pageNumber {
            var someFlag = false
            if self.isFirstCallDone == false {
                self.isFirstCallDone = true
                someFlag = true
            }
            if let c = reviewResponse.reviews?.count, c > 0 {
                self.currentPageNumer = responsePageNumber
                self.totalNumberOfReviews = responseTotalNoOfReviews
                
                if self.currentPageNumer > 0 {
                    let previousCount = self.reviews.count
                    self.reviews.append(contentsOf: (reviewResponse.reviews)!)
                    var items = [IndexPath]()
                    var i = previousCount
                    var capMax = ((reviewResponse.reviews?.count)! + previousCount)
                    if self.isMainRatingFormOpen {
                        i += 1
                        capMax += 1
                    }
                    while i < capMax {
                        items.append(IndexPath(row: i, section: 1))
                        i += 1
                    }
                    if items.count > 0 {
                        self.tableView.beginUpdates()
                        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                        self.tableView.insertRows(at: items, with: .bottom)
                        self.tableView.endUpdates()
                    }
                }
                else {
                    self.isMainRatingFormOpen = false
                    self.reviews.removeAll()
                    if let reviewItems = reviewResponse.reviews {
                        self.reviews.append(contentsOf: reviewItems)
                    }
                    self.tableView.reloadData()
                }
            }
            else {
                if someFlag && reviewResponse.pageNumber == 0 {
                    if responseTotalNoOfReviews > 0 {
                        isMainRatingFormOpen = false
                    }
                    else {
                        isMainRatingFormOpen = true
                    }
                    self.tableView.reloadData()
                }
            }
        }
        //@todo setup header
        self.isOnNetworkCall = false
    }
    
    private func setupViews() {
        self.view.backgroundColor = .white
        
//        self.addToWishlistButton.backgroundColor = Constants.Colors.whiteColor
//        self.addToBagButton.backgroundColor = Constants.Colors.colorFF1744
        
//        self.addtoWishlistButtonTitleLabel.text = "Save"
//        self.addtoWishlistButtonTitleLabel.font = UIFont(name: Constants.FontNames.RubikMedium, size: Constants.FontSize.P4heading)
//        self.addtoWishlistButtonTitleLabel.textColor = Constant.Colors.color8D8D8D
//
//        self.addToCartButtonTitleLabel.text = "Add to bag"
//        self.addToCartButtonTitleLabel.font = UIFont(name: Constants.FontNames.RubikMedium, size: Constant.FontSize.P4heading)
//        self.addToCartButtonTitleLabel.textColor = Constant.Colors.whiteColor
//
        self.setupTableView()
        
//        if self.bottomViewHidden {
//            self.bottomViewBottom.constant = -60.0
//        }
//        self.view.layoutIfNeeded()
    }
    
    private func setupTableView() {
        self.tableView.estimatedRowHeight = 80.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "WriteProductReviewFormTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.WriteProductReviewFormTableViewCell)
        self.tableView.register(UINib(nibName: "UserReviewTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.UserReviewTableViewCell)
        self.tableView.register(UINib(nibName: "RatingPageProductInfoTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.RatingPageProductInfoTableViewCell)
        self.tableView.register(UINib(nibName: "RatingSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: Constants.TableViewCellIdentifier.RatingSectionHeaderView)
    }
    
//    @IBAction func addtoWishlistButtonPressed(_ sender: Any) {
////        self.delegate?.addProductToWishlist(productID: self.productDetail.productListingId!, ussid: self.productDetail.winningUssID!, otherSellers: nil)
//    }
//
//    @IBAction func addtoBagButtonPressed(_ sender: UIButton) {
//        self.delegate?.addProductTocart(productID: self.productDetail.productListingId!, ussid: self.productDetail.winningUssID!, otherSellers: nil)
//    }
    
    @objc private func formCancelButtonAction(_ sender:Any?) {
        self.view.endEditing(true)
//        self.customPickerView.removeFromSuperview()
        self.isMainRatingFormOpen = !self.isMainRatingFormOpen
        let ip = IndexPath(row: 0, section: 1)
        if self.isMainRatingFormOpen {
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [ip], with: UITableViewRowAnimation.top)
            self.tableView.endUpdates()
        }
        else {
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [ip], with: UITableViewRowAnimation.bottom)
            self.tableView.endUpdates()
        }
        self.setHeaderValues()
    }
    
    @objc private func formSubmitButtonAction(_ sender:Any?) {
        self.view.endEditing(true)
//        self.customPickerView.removeFromSuperview()
        if let userReview = self.userReview {
            if userReview.rating! < 1 || userReview.rating! > 5 {
                debugPrint("Please provide rating value.")
                self.showErrorAlert("Please provide rating value.")
                return
            }
            
            if String.isValid(paramString: userReview.headline) == false {
                debugPrint("Please provide review title.")
                self.showErrorAlert("Please provide review title.")
                return
            }
            
            if String.isValid(paramString: userReview.comment) == false {
                debugPrint("Please provide review description.")
                self.showErrorAlert("Please provide review description.")
                return
            }

//            self.showLoader()
            self.isOnNetworkCall = true
            self.request.editOrCreateReview(userReview.headline!, ReviewDescription: userReview.comment!, Rating: userReview.rating!) { [weak self] (reviewResponse, error, httpStatusCode) in
                if self != nil {
//                    self?.hideLoader()
                    if error != nil || reviewResponse == nil {
                        self!.isOnNetworkCall = false
//                        Utility.handleApiNilResponse(withController: self!, theError: error, forGenericResponse: reviewResponse, andRetryCompletion: {
//                            self?.formSubmitButtonAction(sender)
//                        })
                    }
                    else {
                        if reviewResponse?.reviewID != nil {
                            self?.isMainRatingFormOpen = false
                            self?.userReview = ProductReview(json: [:])
                            self?.tableView.beginUpdates()
                            self?.tableView.deleteRows(at: [IndexPath(row: 0, section: 1)], with: .bottom)
                            self?.tableView.endUpdates()
                            let row = self!.tableView.numberOfRows(inSection: 1)
                            reviewResponse?.isJustFreshOne = true
                            self?.reviews.append(reviewResponse!)
                            self?.tableView.beginUpdates()
                            self?.tableView.insertRows(at: [IndexPath(row: row, section: 1)], with: .bottom)
                            self?.tableView.endUpdates()
                            self?.setHeaderValues()
                            
                        }
                    }
                }
            }
            
        }
        else {
            debugPrint("Something went wrong, please try again after some time.")
            self.showErrorAlert("Something went wrong, please try again after some time.")
            return
        }
    }
    
    @objc func textFieldDidChangedEditing(_ textField: UITextField) {
        self.userReview?.headline = textField.text
    }
    
    @objc private func orderByButtonAction(_ sender:Any?) {
        self.view.endEditing(true)
//        self.addPickerView()
    }
    
    @objc private func writeReviewButtonAction(_ sender:Any?) {
//        self.customPickerView.removeFromSuperview()
        self.formCancelButtonAction(sender)
    }
}

extension RatingAndReviewsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 160.0
        }
        else {
            if indexPath.row == 0 && self.isMainRatingFormOpen {
                return 283.0
            }
            return 145.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 53.0
        }
        else {
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.TableViewCellIdentifier.RatingSectionHeaderView) as! RatingSectionHeaderView
            self.rHeaderView = headerView
            self.setHeaderValues()
            let actions = headerView.orderByButton.actions(forTarget: self, forControlEvent: .touchUpInside)
            if actions == nil || actions?.count == 0 {
                headerView.orderByButton.addTarget(self, action: #selector(self.orderByButtonAction(_:)), for: .touchUpInside)
                headerView.writeReviewButton.addTarget(self, action: #selector(self.writeReviewButtonAction(_:)), for: .touchUpInside)
            }
            return headerView
        }
        else {
            return nil
        }
    }
    
    private func setHeaderValues() {
        self.rHeaderView?.orderByLabel.text = self.sortSetting.getDisplayString()
        if self.isMainRatingFormOpen {
            self.rHeaderView?.writeReviewButton.isHidden = true
        }
        else {
            self.rHeaderView?.writeReviewButton.isHidden = false
            let astr = NSMutableAttributedString(string: "Write a review", attributes: [NSAttributedStringKey.font:UIFont(name: Constants.FontNames.RubikRegular, size: 14.0)!, NSAttributedStringKey.foregroundColor: Constants.Colors.color212121, NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
            self.rHeaderView?.writeReviewButton.setAttributedTitle(astr, for: .normal)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && !self.isOnNetworkCall {
            let newPage = self.currentPageNumer + 1
            self.callApi(withLoader: false, pageNumber: newPage)
        }
    }
}

extension RatingAndReviewsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isFirstCallDone {
            return 2
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            var count = self.reviews.count
            if self.isMainRatingFormOpen {
                count += 1
            }
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.RatingPageProductInfoTableViewCell) as! RatingPageProductInfoTableViewCell
            return cell
        }
        else {
            if indexPath.row == 0 && self.isMainRatingFormOpen {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.WriteProductReviewFormTableViewCell) as! WriteProductReviewFormTableViewCell
                cell.headingLabel.text = "Rate this product"
                cell.reviewTitleTextField.placeholder = "Title"
                cell.reviewDescriptionTextView.placeholder = "Write your comments"
                cell.cancelButton.setTitle("Cancel", for: .normal)
                cell.submitButton.setTitle("Submit", for: .normal)
                cell.reviewTitleTextField.delegate = self
                cell.reviewDescriptionTextView.delegate = self
                let actions = cell.cancelButton.actions(forTarget: self, forControlEvent: .touchUpInside)
                if actions == nil || actions?.count == 0 {
                    cell.reviewTitleTextField.addTarget(self, action: #selector(self.textFieldDidChangedEditing(_:)), for: .editingChanged)
                    cell.cancelButton.addTarget(self, action: #selector(self.formCancelButtonAction(_:)), for: .touchUpInside)
                    cell.submitButton.addTarget(self, action: #selector(self.formSubmitButtonAction(_:)), for: .touchUpInside)
                    cell.ratingView.didFinishTouchingCosmos = { rating in
                        self.userReview?.rating = Int(rating)
                    }
                    cell.ratingView.didTouchCosmos = { rating in
                        self.userReview?.rating = Int(rating)
                    }
                }
                cell.reviewTitleTextField.text = self.userReview?.headline
                cell.reviewDescriptionTextView.text = self.userReview?.comment
                var productRating = self.userReview?.rating
                if productRating == nil {
                    productRating = 0
                }
                cell.ratingView.rating = Double(productRating!)
                return cell
            }
            else {
                var index = indexPath.row
                if self.isMainRatingFormOpen {
                    index -= 1
                }
                let model = self.reviews[index]
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.UserReviewTableViewCell) as! UserReviewTableViewCell
                var rValue:Double = 0
                if let r = model.rating {
                    rValue = Double(r)
                }
                cell.ratingView.rating = rValue
                cell.ratingView.isUserInteractionEnabled = false
                cell.reviewTitleLabel.text = model.headline
                cell.reviewDescriptionLabel.text = model.comment
                if model.isJustFreshOne {
                    cell.reviewInfoLabel.text = "Your review is awaiting moderation."
                }
                else {
                    var infoItems = [String]()
                    if let userName = model.alias, String.isValid(paramString: userName) {
                        infoItems.append(userName)
                    }
                    if let cDate = model.reviewDate {
                        let df = DateFormatter()
                        df.dateFormat = "dd MMM, YYYY"
                        infoItems.append(df.string(from: cDate))
                    }
                    cell.reviewInfoLabel.text = infoItems.joined(separator: ", ")
                }
                return cell
            }
        }
    }
}

extension RatingAndReviewsViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.orderOptions[row].getDisplayString()
    }
    
//    override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if self.sortSetting != self.orderOptions[row] {
//            self.sortSetting = self.orderOptions[row]
//            self.callApi(withLoader: true, pageNumber: 0)
//        }
//    }
}

extension RatingAndReviewsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var l = 0
        if let l2 = textField.text?.length {
            l = l2
        }
        if l > 39 && range.length == 0
        {
            return false
        }
        else
        {
            return true
        }
    }
}

extension RatingAndReviewsViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }
}

extension RatingAndReviewsViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.userReview?.comment = textView.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.length > 249 && range.length == 0
        {
            return false
        }
        else
        {
            return true
        }
    }
}
