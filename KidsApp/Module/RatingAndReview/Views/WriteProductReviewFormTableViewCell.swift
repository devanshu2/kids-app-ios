//
//  WriteProductReviewFormTableViewCell.swift
//  TataCLiQ
//
//  Created by Devanshu Saini on 29/03/18.
//  Copyright © 2018 Dew Solutions Private Limited. All rights reserved.
//

import UIKit
import Cosmos

class WriteProductReviewFormTableViewCell: UITableViewCell {

    @IBOutlet weak var headingLabel:UILabel!
    @IBOutlet weak var ratingView:CosmosView!
    @IBOutlet weak var reviewTitleTextField:UITextField!
    @IBOutlet weak var reviewDescriptionTextView:UITextView!
    @IBOutlet weak var cancelButton:UIButton!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var bottomBorderView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.headingLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)!
        self.headingLabel.textColor = Constants.Colors.color181818
        
        self.reviewTitleTextField.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)
        self.reviewTitleTextField.textColor = Constants.Colors.color181818
        
        self.reviewDescriptionTextView.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)!
        self.reviewDescriptionTextView.textColor = Constants.Colors.color181818
        
        self.cancelButton.setTitleColor(Constants.Colors.color8D8D8D, for: .normal)
        self.cancelButton.titleLabel?.font = UIFont(name: Constants.FontNames.RubikMedium, size: 14.0)!
        
        self.submitButton.layer.cornerRadius = 18.0
        self.submitButton.layer.borderWidth = 2.0
        self.submitButton.layer.borderColor = Constants.Colors.color212121.cgColor
        self.submitButton.setTitleColor(Constants.Colors.color212121, for: .normal)
        self.submitButton.titleLabel?.font = UIFont(name: Constants.FontNames.RubikMedium, size: 14.0)!
        
        self.reviewTitleTextField.layer.cornerRadius = 2.0
        self.reviewTitleTextField.layer.borderWidth = 1.0
        self.reviewTitleTextField.layer.borderColor = Constants.Colors.colorD2D2D2.cgColor
        
        self.reviewDescriptionTextView.layer.cornerRadius = 2.0
        self.reviewDescriptionTextView.layer.borderWidth = 1.0
        self.reviewDescriptionTextView.layer.borderColor = Constants.Colors.colorD2D2D2.cgColor
        
        self.bottomBorderView.backgroundColor = Constants.Colors.colorD2D2D2
        
        let lView = UIView(frame: CGRect(x: 0, y: 0, width: 5.0, height: 33.0))
        let rView = UIView(frame: CGRect(x: 0, y: 0, width: 5.0, height: 33.0))
        self.reviewTitleTextField.leftView = lView
        self.reviewTitleTextField.rightView = rView
        self.reviewTitleTextField.leftViewMode = .always
        self.reviewTitleTextField.rightViewMode = .always
        
        self.ratingView.settings.starSize = 30
    }
    
}
