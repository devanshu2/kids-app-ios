//
//  RatingPageProductInfoTableViewCell.swift
//  TataCLiQ
//
//  Created by Devanshu Saini on 29/03/18.
//  Copyright © 2018 Dew Solutions Private Limited. All rights reserved.
//

import UIKit
import Cosmos

class RatingPageProductInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageViewWrapper:UIView!
    @IBOutlet weak var productImageView:UIImageView!
    @IBOutlet weak var productNameLabel:UILabel!
    @IBOutlet weak var productBrandLabel:UILabel!
    @IBOutlet weak var productPriceLabel:UILabel!
    @IBOutlet weak var ratingView:CosmosView!
    @IBOutlet weak var ratingCountLabel:UILabel!
    @IBOutlet weak var ratingInfoLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.productImageViewWrapper.layer.cornerRadius = 4.0
        
        self.productNameLabel.font = UIFont(name: Constants.FontNames.RubikMedium, size: 16.0)!
        self.productNameLabel.textColor = Constants.Colors.color181818
        
        self.productBrandLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 16.0)!
        self.productBrandLabel.textColor = Constants.Colors.color181818
        
        self.ratingCountLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 16.0)!
        self.ratingCountLabel.textColor = Constants.Colors.color181818
        
        self.productPriceLabel.font = UIFont(name: Constants.FontNames.RubikMedium, size: 16.0)!
        self.productPriceLabel.textColor = Constants.Colors.color181818
        
        self.ratingInfoLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)!
        self.ratingInfoLabel.textColor = Constants.Colors.color8D8D8D
    }
    
}
