//
//  UserReviewTableViewCell.swift
//  TataCLiQ
//
//  Created by Devanshu Saini on 29/03/18.
//  Copyright © 2018 Dew Solutions Private Limited. All rights reserved.
//

import UIKit
import Cosmos

class UserReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingView:CosmosView!
    @IBOutlet weak var reviewTitleLabel:UILabel!
    @IBOutlet weak var reviewDescriptionLabel:UILabel!
    @IBOutlet weak var reviewInfoLabel:UILabel!
    @IBOutlet weak var bottomBorderView:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.reviewTitleLabel.font = UIFont(name: Constants.FontNames.RubikMedium, size: 14.0)!
        self.reviewTitleLabel.textColor = Constants.Colors.color000000
        
        self.reviewDescriptionLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)!
        self.reviewDescriptionLabel.textColor = Constants.Colors.color000000
        
        self.reviewInfoLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)!
        self.reviewInfoLabel.textColor = Constants.Colors.color8D8D8D
        
        self.bottomBorderView.backgroundColor = Constants.Colors.colorD2D2D2
    }
}
