//
//  RatingSectionHeaderView.swift
//  TataCLiQ
//
//  Created by Devanshu Saini on 29/03/18.
//  Copyright © 2018 Dew Solutions Private Limited. All rights reserved.
//

import UIKit

class RatingSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var orderByLabel:UILabel!
    @IBOutlet weak var orderByWrapper:UIView!
    @IBOutlet weak var orderByButton:UIButton!
    @IBOutlet weak var writeReviewButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.orderByLabel.font = UIFont(name: Constants.FontNames.RubikLight, size: 14.0)!
        self.orderByLabel.textColor = Constants.Colors.color212121
        
        self.orderByWrapper.layer.cornerRadius = 2.0
        self.orderByWrapper.layer.borderWidth = 1.0
        self.orderByWrapper.layer.borderColor = Constants.Colors.colorD2D2D2.cgColor
    }
}
