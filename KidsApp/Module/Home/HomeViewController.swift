//
//  HomeViewController.swift
//  KidsApp
//
//  Created by Devanshu Saini on 14/12/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit
import MapKit

class HomeViewController: BaseViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var mapView:MKMapView!
    fileprivate let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.showsUserLocation = true
        self.mapView.showsCompass = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func applicationDidBecomeActive() {
        self.callApi(forParent: 0, place: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkLocationAuthorizationStatus()
        
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = .red
//        UINavigationBar.appearance().titl
        self.title = "Home"
        self.callApi(forParent: 0, place: nil)
    }
    
    
    
    private func callApi(forParent:Int32?, place:PlaceItem?) {
        let pid = forParent == nil ? place?.placeID : forParent
        self.showLoader()
        HomeDataRequest().getPlacesData(Parent: pid!) { [unowned self] (response, error) in
            self.hideLoader()
            if error != nil {
                Utils.handleApiNilResponse(withController: self, theError: error, andRetryCompletion: {
                    self.callApi(forParent: forParent, place: place)
                })
            }
            else {
                if forParent == 0 {
                    self.mapView.removeAnnotations(self.mapView.annotations)
                    if let places = response?.places {
                        self.mapView.addAnnotations(places)
                        self.mapView.showAnnotations(places, animated: true)
                    }
                }
                else {
                    if let places = response?.places, places.count > 0 {
                        response?.theTitle = place?.placeName
                        self.performSegue(withIdentifier: "toList", sender: response) //homeToDetail
                    }
                    else {
                        if place != nil {
                            self.performSegue(withIdentifier: "homeToDetail", sender: place) //homeToDetail
                        }
                    }
                }
            }
        }
    }

    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ListViewController, let placeResponse = sender as? PlacesResponse {
            vc.topTitle = placeResponse.theTitle
            vc.places = placeResponse.places
        }
        else if let vc = segue.destination as? DetailViewController, let place = sender as? PlaceItem {
            vc.place = place
        }
    }
}

extension HomeViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? PlaceItem else { return nil }
        var imgURL: URL?
        if let urlString = annotation.imageString {
            imgURL = URL(string: urlString)
        }
        // 3
        let identifier = "marker"
        if #available(iOS 11.0, *) {
            var view: CustomMarkerAnnotationView
            // 4
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? CustomMarkerAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            }
            else {
                // 5
                view = CustomMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            
            view.setImage(with: imgURL, placeHolder: #imageLiteral(resourceName: "beenhere"))
//            view.glyphImage =
            return view
        } else {
            // Fallback on earlier versions
            var view: CustomMKAnnotationView
            // 4
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? CustomMKAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            }
            else {
                // 5
                view = CustomMKAnnotationView(annotation: annotation, reuseIdentifier: identifier) //MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            view.setImage(with: imgURL, placeHolder: #imageLiteral(resourceName: "beenhere"))
//            view.image = #imageLiteral(resourceName: "beenhere")
            return view
        }
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let location = view.annotation as? PlaceItem {
            self.callApi(forParent: nil, place: location)
        }
        debugPrint("going for list")
    }
}

