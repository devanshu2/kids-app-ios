//
//  HomeData.swift
//  SpecialDeal
//
//  Created by Devanshu Saini on 24/11/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//
import Gloss
import MapKit
//import CoreData

public class MarkerImage: NSObject, JSONDecodable {
    public var imageID: Int32?
    public var markerID: Int32?
    public var image: String?
    public var imageURLString: String? {
        get {
            if let imgs = self.image {
                return Constants.APIConstants.Base + "uploads/marker/" + imgs
            }
            else {
                return nil
            }
        }
    }
    public var featured: Int16?
    public var created: Date?
    
    required public init?(json: JSON) {
        super.init()
        self.imageID = "id" <~~ json
        self.markerID = "marker_id" <~~ json
        self.image = "images" <~~ json
        self.featured = "featured" <~~ json
        let createdResponse:String? = "created" <~~ json
        self.created = Date.dateFromAPIDateResponse(createdResponse)
    }
}

public class PlaceItem: NSObject, JSONDecodable, MKAnnotation {
    public var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
        }
    }
    
    public var title: String? {
        get {
            return self.placeName
        }
    }
    
    public var avgRating: Int16?
    public var placeID: Int32?
    public var parentID: Int32?
    public var placeName: String?
    public var subTitle: String?
    public var itemDescription: String?
    public var image: String?
    public var imageString: String? {
        get {
            if let imgs = self.image {
                return Constants.APIConstants.Base + "uploads/marker/" + imgs
            }
            return nil
        }
    }

    public var sortOrder: Int16?
    public var latitude: Double?
    public var longitude: Double?
    public var status: Int16?
    public var created: Date?
    public var modified: Date?
    public var markerImages:[MarkerImage]?

    public var featuredMarkerImage: MarkerImage? {
        get {
            if let items = self.markerImages, items.count > 0 {
                let filtered = items.filter { (singleItem) -> Bool in
                    if singleItem.featured == 1 {
                        return true
                    }
                    else {
                        return false
                    }
                }
                if filtered.count > 0 {
                    return filtered.first
                }
                else {
                    return items.first
                }
            }
            return nil
        }
    }
    
    required public init?(json: JSON) {
        super.init()
        self.avgRating = "avgRating" <~~ json
        self.placeID = "id" <~~ json
        self.parentID = "parent_id" <~~ json
        self.placeName = "name" <~~ json
        self.subTitle = "sub_title" <~~ json
        self.itemDescription = "description" <~~ json
        self.image = "image" <~~ json
        self.sortOrder = "sort_order" <~~ json
        if self.sortOrder == nil {
            self.sortOrder = 0
        }
        self.latitude = "latitude" <~~ json
        self.longitude = "longitude" <~~ json
        if self.latitude == nil {
            self.latitude = 0.0
        }
        if self.longitude == nil {
            self.longitude = 0.0
        }
        self.status = "status" <~~ json
        let createdResponse:String? = "created" <~~ json
        self.created = Date.dateFromAPIDateResponse(createdResponse)
        let modifiedResponse:String? = "modified" <~~ json
        self.modified = Date.dateFromAPIDateResponse(modifiedResponse)
        self.markerImages = "marker_images" <~~ json
    }
}

public class PlacesResponse: BaseResponse {
    public var places:[PlaceItem]?
    public var theTitle: String?
    
    public required init?(json: JSON) {
        super.init(json: json)
        self.places = "places" <~~ json
    }
}
//
//class HomeDataResponse: BaseResponse {
//    public var items:[DealItem]?
//    
//    public func syncDataBase(_ completionHandler: @escaping () -> Swift.Void) {
//        if let deals = self.items {
//            DispatchQueue.main.async {
//                let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.Entities.DealProduct)
//                let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//                do {
//                    try appdelegate.persistentContainer.viewContext.execute(deleteRequest)
//                    for deal in deals {
//                        let element:DealProduct = NSEntityDescription.insertNewObject(forEntityName: Constants.Entities.DealProduct, into: appdelegate.persistentContainer.viewContext) as! DealProduct
//                        element.product_id = deal.product_id!
//                        element.product_title = deal.product_title
//                        element.product_descriptiom = deal.product_descriptiom
//                        element.product_sort_order = (deal.product_sort_order == nil) ? 0 : deal.product_sort_order!
//                        element.product_image = deal.product_image
//                        element.product_code = deal.product_code
//                        element.product_price = (deal.product_price == nil) ? "0" : deal.product_price!
//                        do {
//                            try appdelegate.persistentContainer.viewContext.save()
//                        }
//                        catch {
//                            let nserror = error as NSError
//                            #if DEBUG
//                                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//                            #else
//                                debugPrint("Unresolved error \(nserror), \(nserror.userInfo)")
//                            #endif
//                        }
//                    }
//                }
//                catch {
//                    let nserror = error as NSError
//                    #if DEBUG
//                        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//                    #else
//                        debugPrint("Unresolved error \(nserror), \(nserror.userInfo)")
//                    #endif
//                }
//                completionHandler()
//            }
//        }
//        else {
//            completionHandler()
//        }
//    }
//    
//    required init?(json: JSON) {
//        
//        super.init(json: json)
//        self.items = "data" <~~ json
//    }
//}
//
class HomeDataRequest: BaseModel {

    func getPlacesData(Parent parentID:Int32? = nil, CompletionHandler completionHandler: @escaping (PlacesResponse?, Error?) -> Swift.Void){
        var apiEndPoint = Constants.APIConstants.EndPoints.places
        if parentID != nil {
            apiEndPoint.append("?parent=\(parentID!)")
        }
        self.networkManager.getData(withApiEndpoint: apiEndPoint) { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                DispatchQueue.main.async {
                    completionHandler(nil, error)
                }
            }
            else {
                do{
                    let responseData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    guard let jsonDict = PlacesResponse(json: responseData!) else {
                        debugPrint("Could not convert json into data model")
                        self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
                        DispatchQueue.main.async {
                            completionHandler(nil, error)
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        completionHandler(jsonDict, error)
                    }
                }
                catch {
                    self.recordAPIParseIssue(data: data, endpoint: apiEndPoint, function: #function, file: #file, line: #line)
                    debugPrint(error.localizedDescription)
                    DispatchQueue.main.async {
                        completionHandler(nil, error)
                    }
                }
            }
        }
    }
}

