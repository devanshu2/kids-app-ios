import Foundation
import Gloss

public class BaseModel: NSObject {
    public lazy var networkManager = NetworkManger()
    
    public func recordAPIParseIssue(data:Data?, endpoint:String, function:String?, file:String?, line:Int?) {
        Utils.recordAPIParseIssue(data: data, endpoint: endpoint, function: function, file: file, line: line, otherInfo: nil)
    }
}

public class BaseResponse: NSObject, Glossy {
    public var success:Bool?
    required public init?(json: JSON) {
        super.init()
        self.success = "success" <~~ json
        if self.success == nil {
            self.success = false
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([])
    }
}
