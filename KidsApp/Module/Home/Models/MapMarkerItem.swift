//
//  MapMarkerItem.swift
//  KidsApp
//
//  Created by Devanshu Saini on 17/12/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

//to remove

import UIKit
import MapKit

public class MapMarkerItemWithDetails: NSObject {
    public var coordinate:CLLocationCoordinate2D!
    public var title:String?
    public var subtitle: String?
    public var iconImage:UIImage?
}

public class MapMarkerItem: NSObject, MKAnnotation {
    public var coordinate: CLLocationCoordinate2D {
        get {
            return self.locationData.coordinate
        }
    }
    
    public var title: String? {
        get {
            return self.locationData.title
        }
    }
    
    public var subtitle: String? {
        get{
            return self.locationData.subtitle
        }
    }
    
    public var locationData:MapMarkerItemWithDetails!
    
    
    required public init(withModel model:MapMarkerItemWithDetails) {
        super.init()
        self.locationData = model
    }
    
    // annotation callout opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let placemark = MKPlacemark(coordinate: self.coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
