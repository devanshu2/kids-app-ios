//
//  ImageCollectionCell.swift
//  Tromsa
//
//  Created by Devanshu Saini on 25/05/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var checkImageView:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkImageView.isHidden = true
    }

}
