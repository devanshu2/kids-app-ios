//
//  PlaceDescriptionTableViewCell.swift
//  KidsApp
//
//  Created by Devanshu Saini on 21/09/18.
//  Copyright © 2018 Devanshu Saini. All rights reserved.
//

import UIKit

class PlaceDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var bottomTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
