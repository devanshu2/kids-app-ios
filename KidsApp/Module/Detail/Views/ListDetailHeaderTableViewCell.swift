//
//  ListDetailHeaderTableViewCell.swift
//  Tromsa
//
//  Created by Devanshu Saini on 12/06/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit

class ListDetailHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let horizontalLayout = UICollectionViewFlowLayout()
        horizontalLayout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = horizontalLayout
        self.collectionView.isPagingEnabled = true
        self.collectionView.register(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: Constants.TableViewCellIdentifier.ImageCollectionCell)
    }
    
}
