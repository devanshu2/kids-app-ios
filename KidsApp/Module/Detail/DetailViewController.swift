//
//  DetailViewController.swift
//  KidsApp
//
//  Created by Devanshu Saini on 14/12/17.
//  Copyright © 2017 Devanshu Saini. All rights reserved.
//

import UIKit
import BFRImageViewer
import SDWebImage

class DetailViewController: BaseViewController {
    
    @IBOutlet weak var tableView:UITableView!
    public var place:PlaceItem!    
    private var collectionHeight: CGFloat = 190

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.place.placeName
        self.tableView.register(UINib(nibName: "ListDetailHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.ListDetailHeaderTableViewCell)
        self.tableView.register(UINib(nibName: "PlaceDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.PlaceDescriptionTableViewCell)
    }
    
}

extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.ListDetailHeaderTableViewCell) as! ListDetailHeaderTableViewCell
            cell.collectionView.dataSource = self
            cell.collectionView.delegate = self
            cell.collectionViewHeight.constant = self.collectionHeight
            cell.collectionView.reloadData()
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.PlaceDescriptionTableViewCell) as! PlaceDescriptionTableViewCell
            if String.isValid(paramString: self.place.subTitle) {
                cell.topTitleLabel.text = self.place.subTitle
                cell.topTitleLabel.isHidden = false
            }
            else {
                cell.topTitleLabel.isHidden = true
            }
            cell.bottomTitleLabel.text = self.place.itemDescription
            return cell
        }
        return UITableViewCell()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension DetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let c = self.place.markerImages?.count {
            if c > 0 {
                var selectedIndex = 0
                var urls = [URL]()
                var counter = 0
                for i in 0..<c {
                    if let imageString = self.place.markerImages?[i].imageURLString {
                        if let url = URL(string: imageString) {
                            urls.append(url)
                            if i == indexPath.item {
                                selectedIndex = counter
                            }
                            counter += 1
                        }
                    }
                }
                if urls.count > 0 {
                    let imageVC = BFRImageViewController(imageSource: urls)
                    imageVC?.startingIndex = selectedIndex
                    self.present(imageVC!, animated: true, completion: nil)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return .zero
    }
}

// MARK: - UICollectionViewDataSource
extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let c = self.place.markerImages?.count {
            return c
        }
        else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.TableViewCellIdentifier.ImageCollectionCell, for: indexPath) as! ImageCollectionCell
        cell.imageView.contentMode = .scaleAspectFill
        cell.imageView.image = #imageLiteral(resourceName: "default")
        if let c = self.place.markerImages?.count {
            if c > 0 {
                let model = self.place.markerImages?[indexPath.row]
                var imgURL: URL?
                if let imageString = model?.imageURLString {
                    if let url = URL(string: imageString) {
                        imgURL = url
                    }
                }
                cell.imageView.sd_setShowActivityIndicatorView(true)
                cell.imageView.sd_setIndicatorStyle(.gray)
                cell.imageView.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "default"), options: SDWebImageOptions.refreshCached) { [weak self] (image, error, cacheType, url) in
                    guard let `self` = self else { return }
                    if let img = image {
                        let cw = collectionView.bounds.size.width
                        let nH = (img.size.height * cw)/img.size.width
                        var newHeight: CGFloat = 190
                        if nH > newHeight {
                            let h70 = self.tableView.bounds.size.height * 0.7
                            if nH > h70 {
                                newHeight = h70
                            }
                            else {
                                newHeight = nH
                            }
                        }
                        var animation = UITableViewRowAnimation.bottom
                        if self.collectionHeight > newHeight {
                            animation = .top
                        }
                        else if self.collectionHeight == newHeight {
                            return
                        }
                        self.collectionHeight = newHeight
                        DispatchQueue.main.async {
                            cell.imageView.image = img
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: animation)
                            self.tableView.endUpdates()
                        }
                    }
                    else {
                        debugPrint(error?.localizedDescription ?? "")
                    }
                }
            }
        }
        return cell
    }
}


extension DetailViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView is UICollectionView &&  decelerate == false {
            let currentPage = scrollView.currentPage
            // Do something with your page update
            debugPrint("scrollViewDidEndDragging: \(currentPage)")
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView is UICollectionView {
            let currentPage = scrollView.currentPage
            // Do something with your page update
            debugPrint("scrollViewDidEndDragging: \(currentPage)")
        }
    }
}
